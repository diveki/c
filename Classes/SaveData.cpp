/*
 * SaveData.cpp
 *
 *  Created on: 8 May 2015
 *      Author: zsoltlinux
 */

#ifndef SAVEDATA_CPP_
#define SAVEDATA_CPP_

#include "SaveData.hpp"
#include <fstream>
#include <iostream>

//template <class V>
SaveData::SaveData() {
	// TODO Auto-generated constructor stub

}

SaveData::~SaveData() {
	// TODO Auto-generated destructor stub
}

template <class V>
void mysave(const std::string& pname, const V& data) {
	std::ofstream myfile;
	myfile.open(pname.c_str());
	myfile << data;
	myfile.close();
	std::cout << "====== File is saved! ======\nPathname: " << pname << std::endl;
}

template <class V>
void mysave(const std::string& pname, V& data) {
	std::ofstream myfile;
	myfile.open(pname.c_str());
	myfile << data;
	myfile.close();
	std::cout << "====== File is saved! ======\nPathname: " << pname << std::endl;
}

#endif
