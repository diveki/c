/*
 * Plotting.cpp
 *
 *  Created on: 18 May 2015
 *      Author: zsoltlinux
 */

#ifndef PLOTTING_CPP_
#define PLOTTING_CPP_

#include "Plotting.hpp"

Gnuplot::Gnuplot() {
	// TODO Auto-generated constructor stub
	Gnuplot::init();
}

template <class V>
Gnuplot::Gnuplot(V& p) {
	this->PlotData = "data.txt";
	mysave(this->PlotData,p);
	Gnuplot::init();
}

Gnuplot::~Gnuplot() {
	// TODO Auto-generated destructor stub

}

void Gnuplot::init() {
	if (!getenv("DISPLAY"))
		putenv("DISPLAY=:0.0");

	gp.push_back("set term wxt");
}

void Gnuplot::ExportScript(char tmp[] = "GPscript.gp") {
	mysave(tmp,(*this));
}

void Gnuplot::Plot(char str[] = "GPscript.gp") {
	int i;
	char tmp [50];
	i=sprintf(tmp, "gnuplot -persist > load %s",str);
	i=system (tmp);
	printf ("The value returned was: %d.\n",i);
}

void Gnuplot::AddLine(const std::string& str) {
	this->gp.push_back(str);
}

void Gnuplot::operator + (const std::string& str) {
	return this->AddLine(str);
}

std::ostream& operator << (std::ostream& os, Gnuplot& p)
{ // Output to screen

	for (auto i = p.gp.cbegin(); i != p.gp.cend(); ++i) {
		os << *i << "\n";
	}
	return os;
}



#endif
