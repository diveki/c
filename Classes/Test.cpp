#include <iostream>
#include <list>
#include <string>
#include "SaveData.cpp"
#include "TestC.cpp"
#include "Plotting.cpp"
#include "NonlinearSolver/NewtonRaphsonSolver.cpp"
#include "NonlinearSolver/SteffensenSolver.cpp"
#include "NonlinearSolver/BiSectionSolver.cpp"
#include <cmath>

double Function(double x) {
	double sum=0;
	for (int i = 1; i<=10; i++) {
		sum = sum + 100/std::pow((1+x),i);
	}
	return 772.173-sum;
}

double DerFunction(double x) {
	double sum = 0;
	for (int i = 1; i<=10; i++) {
		sum = sum + 100*i/std::pow((1+x),(i+1));
	}
	return -sum;
}

int main () {
	double x = 0.04;
//	std::cout << Function(x);
	NewtonRaphsonSolver s(x,Function,DerFunction);
	SteffensenSolver s1(x,Function);
	BiSectionSolver s2(0.5, 1, Function);
//	s.Set_MAXIter(1e4);
//	s1.Set_MAXIter(1e4);

//	x = s.solve();
//	s.printStatistics();

	x = s2.solve();
	s2.printStatistics();

	/*	Gnuplot test;
	std::string tmp = "plot x**2";

	test + tmp;
	test.ExportScript("test.gp");

	  printf ("Checking if processor is available...");
	  if (system(NULL)) puts ("Ok");
	    else exit (EXIT_FAILURE);


//	  i=system ("gnuplot -persist load /home/zsoltlinux/Programok/C++/Geometry/GPscript.gp");
	  test.Plot("test.gp");
*/
	  return 0;
}

