/*
 * SaveData.hpp
 *
 *  Created on: 8 May 2015
 *      Author: zsoltlinux
 */

#ifndef SAVEDATA_HPP_
#define SAVEDATA_HPP_

#include<string>

//template <class V>
class SaveData {
public:
	inline SaveData();
	inline virtual ~SaveData();


};

template <class V>
inline void mysave(const std::string& pname, const V& data);
template <class V>
inline void mysave(const std::string& pname, V& data);

#endif /* SAVEDATA_HPP_ */
