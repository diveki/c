/*
 * BiSectionSolver.cpp
 *
 *  Created on: 29 May 2015
 *      Author: zsoltlinux
 */

#ifndef BISECTIONSOLVER_CPP_
#define BISECTIONSOLVER_CPP_

#include "BiSectionSolver.hpp"
#include <cmath>
#include <iostream>

BiSectionSolver::BiSectionSolver(const double& guessA, const double& guessB, double (*myFunc)(double x)) {
	// TODO Auto-generated constructor stub
	init(guessA,guessB,myFunc);

}

BiSectionSolver::~BiSectionSolver() {
	// TODO Auto-generated destructor stub
}

void BiSectionSolver::init(const double& guessA, const double& guessB, double (*myFunc)(double x)) {
	this->Set_StartingPoint(guessA);
	this->Set_PreviousPoint(guessB);
	myF = myFunc;
}

double BiSectionSolver::solve() {
	tbegin = clock();
	double tmp=0, tn=0;
	this->Set_CurrentPoint(this->Get_StartinPoint());
	double fn = myF(this->Get_CurrentPoint());
	double gn = myF(this->Get_PreviousPoint());
	if (fn*gn > 0) {
		this->error_msg(this->name,__func__,"'x0' or 'xPrevious'","ERROR: The starting points are not properly chosen.");
		tend = clock();
		this->Set_ElapsedTime(tbegin,tend);
		return this->Get_StartinPoint();
	}
	else {
		iter_counter = 0;
		while (true) {
			iter_counter = iter_counter + 1;

			if (std::abs(this->Get_CurrentPoint() - this->Get_PreviousPoint()) < this->Get_Tolerance()) {
				tend = clock();
				this->Set_ElapsedTime(tbegin,tend);
				return this->Get_CurrentPoint();
			}
			else {
				tmp = (this->Get_PreviousPoint() + this->Get_CurrentPoint())/2;
				tn = myF(tmp);
				if ((tn*fn > 0) && (tn*gn < 0)) {
					this->Set_CurrentPoint(tmp);
				}
				if ((tn*fn < 0) && (tn*gn > 0)) {
					this->Set_PreviousPoint(tmp);
				}

				if (iter_counter > this->Get_MAXIter()) {
					this->error_msg(this->name,__func__,"iter_counter","ERROR: The solution procedure reached the MAX_ITERATION or CHANGE initial point.");
					tend = clock();
					this->Set_ElapsedTime(tbegin,tend);
					return this->Get_CurrentPoint();
				}
			}
		}
	}

}

void BiSectionSolver::printStatistics() {
	NonlinearSolver::printStatistics();
}

#endif
