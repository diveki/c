/*
 * NonlinearSolver.hpp
 *
 *  Created on: 29 May 2015
 *      Author: zsoltlinux
 */

#ifndef NONLINEARSOLVER_HPP_
#define NONLINEARSOLVER_HPP_

#include<string>
#include<iostream>
#include <ctime>

class NonlinearSolver {
private:
	double x0, xPrevious, xCurrent, tolerance=1e-5, elapsedt=0;
	long MAX_ITERATION=1000;

//	inline void init(const double& guess, double (*myFunc)(double x));

public:
	long iter_counter;
	double (*myF)(double x);
	std::string name;
	std::clock_t tbegin, tend;

//	inline NonlinearSolver();
//	inline virtual ~NonlinearSolver();

	// Setters
	inline virtual void Set_StartingPoint(const double& x) { x0 = x; }
	inline virtual void Set_Tolerance(const double& x) { tolerance = x; }
	inline virtual void Set_MAXIter(const long& x) { MAX_ITERATION = x; }
	inline virtual void Set_PreviousPoint(const double& x) { xPrevious = x; }
	inline virtual void Set_CurrentPoint(const double& x) { xCurrent = x; }
	inline virtual void Set_ElapsedTime(const std::clock_t& begin, const std::clock_t& end) { elapsedt = double(end - begin) / CLOCKS_PER_SEC; }

	// Getters
	inline virtual double Get_Tolerance() const { return tolerance; }
	inline virtual double Get_StartinPoint() const { return x0; }
	inline virtual double Get_PreviousPoint() const { return xPrevious; }
	inline virtual double Get_CurrentPoint() const { return xCurrent; }
	inline virtual long Get_MAXIter() const { return MAX_ITERATION; }
	inline virtual double Get_ElapsedTime() const { return elapsedt; }

	// Methods
	inline virtual double solve()=0;
	inline virtual void printStatistics() {
		std::cout << "======= Statistics about the Nonlinear equation solution using: " << this->name << " solver ========\n" << std::endl;
		std::cout << "Number of current iterations: " << this->iter_counter << std::endl;
		std::cout << "The current value of the solution: " << this->Get_CurrentPoint() << std::endl;
		std::cout << "The current elapsed time: " << this->Get_ElapsedTime() << " seconds"<< std::endl;
		std::cout << "--- Condition for stopping the iteration --- : "  << std::endl;
		std::cout << "Max. iteration = " << this->Get_MAXIter() << ",\tTolerance = " << this->Get_Tolerance() << std::endl;
	}


};

#endif /* NONLINEARSOLVER_HPP_ */
