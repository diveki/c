/*
 * SteffensenSolver.hpp
 *
 *  Created on: 29 May 2015
 *      Author: zsoltlinux
 */

#ifndef STEFFENSENSOLVER_HPP_
#define STEFFENSENSOLVER_HPP_

#include "NonlinearSolver.hpp"
#include "../ErrorClass.hpp"

class SteffensenSolver : public NonlinearSolver, ErrorClass {
private:
	inline void init(const double& guess, double (*myFunc)(double x));

public:
	std::string name = "Steffensen";

	inline SteffensenSolver(const double& guess, double (*myFunc)(double x));
	inline virtual ~SteffensenSolver();

	inline virtual double solve();
	inline virtual void printStatistics();
};

#endif /* STEFFENSENSOLVER_HPP_ */
