/*
 * NEWTONRAPHSONSolver.hpp
 *
 *  Created on: 29 May 2015
 *      Author: zsoltlinux
 */

#ifndef NEWTONRAPHSONSOLVER_HPP_
#define NEWTONRAPHSONSOLVER_HPP_

#include "NonlinearSolver.hpp"
#include "../ErrorClass.hpp"

class NewtonRaphsonSolver : public NonlinearSolver, ErrorClass {
private:
	inline void init(const double& guess, double (*myFunc)(double x), double (*myDerFunc)(double x));

public:
	std::string name = "NewtonRaphson";
	double (*myDerF)(double x);

	inline NewtonRaphsonSolver(const double& guess, double (*myFunc)(double x), double (*myDerFunc)(double x));
	inline virtual ~NewtonRaphsonSolver();

	inline double solve();
	inline void printStatistics();
};

#endif /* NewtonRaphsonSOLVER_HPP_ */
