/*
 * NewtonRaphsonSolver.cpp
 *
 *  Created on: 29 May 2015
 *      Author: zsoltlinux
 */

#ifndef NEWTONRAPHSONSOLVER_CPP_
#define NEWTONRAPHSONSOLVER_CPP_

#include "NewtonRaphsonSolver.hpp"
#include <cmath>
#include <iostream>

NewtonRaphsonSolver::NewtonRaphsonSolver(const double& guess, double (*myFunc)(double x), double (*myDerFunc)(double x)) {
	// TODO Auto-generated constructor stub
	init(guess,myFunc, myDerFunc);

}

NewtonRaphsonSolver::~NewtonRaphsonSolver() {
	// TODO Auto-generated destructor stub
}

void NewtonRaphsonSolver::init(const double& guess, double (*myFunc)(double x), double (*myDerFunc)(double x)) {
	this->Set_StartingPoint(guess);
	this->Set_PreviousPoint(guess);
	myF = myFunc;
	myDerF = myDerFunc;
}

double NewtonRaphsonSolver::solve() {
	tbegin = clock();
	double fn = myF(this->Get_StartinPoint());
	double gn=myDerF(this->Get_StartinPoint()), tmp=0;
	iter_counter = 0;
	while (true) {
//		gn = (myF(this->Get_PreviousPoint() + fn) - fn)/fn;
		tmp = this->Get_PreviousPoint() - (fn/gn);
		this->Set_CurrentPoint(tmp);
		iter_counter = iter_counter + 1;

		if (std::abs(this->Get_CurrentPoint() - this->Get_PreviousPoint()) < this->Get_Tolerance()) {
			tend = clock();
			this->Set_ElapsedTime(tbegin,tend);
			return this->Get_CurrentPoint();
		}
		else {
			this->Set_PreviousPoint(this->Get_CurrentPoint());
			fn = myF(this->Get_PreviousPoint());
			gn = myDerF(this->Get_PreviousPoint());
		}

		if (iter_counter > this->Get_MAXIter()) {
			this->error_msg(this->name,__func__,"'iter_counter' or 'x0'","ERROR: The solution procedure reached the MAX_ITERATION or CHANGE initial point.");
			tend = clock();
			this->Set_ElapsedTime(tbegin,tend);
			return this->Get_CurrentPoint();
		}
	}
}

void NewtonRaphsonSolver::printStatistics() {
	NonlinearSolver::printStatistics();
}

#endif
