/*
 * SteffensenSolver.cpp
 *
 *  Created on: 29 May 2015
 *      Author: zsoltlinux
 */

#ifndef STEFFENSENSOLVER_CPP_
#define STEFFENSENSOLVER_CPP_

#include "SteffensenSolver.hpp"
#include <cmath>
#include <iostream>

SteffensenSolver::SteffensenSolver(const double& guess, double (*myFunc)(double x)) {
	// TODO Auto-generated constructor stub
	init(guess,myFunc);

}

SteffensenSolver::~SteffensenSolver() {
	// TODO Auto-generated destructor stub
}

void SteffensenSolver::init(const double& guess, double (*myFunc)(double x)) {
	this->Set_StartingPoint(guess);
	this->Set_PreviousPoint(guess);
	myF = myFunc;
}

double SteffensenSolver::solve() {
	tbegin = clock();
	double fn = myF(this->Get_StartinPoint());
	std::cout << "Starting point=" << this->Get_StartinPoint() << "fn= " << fn << std::endl;
	double gn=0, tmp=0;
	iter_counter = 0;
	while (true) {
		gn = (myF(this->Get_PreviousPoint() + fn) - fn)/fn;
		tmp = this->Get_PreviousPoint() - (fn/gn);
		this->Set_CurrentPoint(tmp);
		iter_counter = iter_counter + 1;

		std::cout << "fn= " << fn << ", x= " << this->Get_CurrentPoint() << std::endl;

		if (std::abs(this->Get_CurrentPoint() - this->Get_PreviousPoint()) < this->Get_Tolerance()) {
			tend = clock();
			this->Set_ElapsedTime(tbegin,tend);
			return this->Get_CurrentPoint();
		}
		else {
			this->Set_PreviousPoint(this->Get_CurrentPoint());
			fn = myF(this->Get_PreviousPoint());
		}

		if (iter_counter > this->Get_MAXIter()) {
			this->error_msg(this->name,__func__,"iter_counter","ERROR: The solution procedure reached the MAX_ITERATION or CHANGE initial point.");
			tend = clock();
			this->Set_ElapsedTime(tbegin,tend);
			return this->Get_CurrentPoint();
		}
	}
}

void SteffensenSolver::printStatistics() {
	NonlinearSolver::printStatistics();
}

#endif
