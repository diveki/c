/*
 * BiSectionSolver.hpp
 *
 *  Created on: 29 May 2015
 *      Author: zsoltlinux
 */

#ifndef BISECTIONSOLVER_HPP_
#define BISECTIONSOLVER_HPP_

#include "NonlinearSolver.hpp"
#include "../ErrorClass.hpp"

class BiSectionSolver : public NonlinearSolver, ErrorClass {
private:
	inline void init(const double& guessA, const double& guessB, double (*myFunc)(double x));

public:
	std::string name = "Bisection";

	inline BiSectionSolver(const double& guessA, const double& guessB, double (*myFunc)(double x));
	inline virtual ~BiSectionSolver();

	inline virtual double solve();
	inline virtual void printStatistics();
};

#endif /* BISECTIONSOLVER_HPP_ */
