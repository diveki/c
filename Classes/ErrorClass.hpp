/*
 * ErrorClass.hpp
 *
 *  Created on: 6 May 2015
 *      Author: zsoltlinux
 */

#ifndef ERRORCLASS_HPP_
#define ERRORCLASS_HPP_
#include <string>
#include <iostream>

class ErrorClass {
public:
//	ErrorClass();
//	virtual ~ErrorClass();

	// Methods
	void error_msg(const std::string& classname, const std::string& functionname, const std::string& varname, const std::string& msg) const {
		std::cout << "====== Error Message! ======" << std::endl;
		std::cout << "Error occurred in: " << std::endl;
		std::cout << "\tClass: " << classname << std::endl;
		std::cout << "\tFunction: " << functionname << std::endl;
		std::cout << "\tVariable: " << varname << "\n" << std::endl;
		std::cout << "Suggestion to resolve the problem: " << std::endl;
		std::cout << "\t" << msg << std::endl;
		std::cout << "============================" << std::endl;
	}
};

#endif /* ERRORCLASS_HPP_ */
