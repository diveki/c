/*
 * TestH.hpp
 *
 *  Created on: 11 May 2015
 *      Author: zsoltlinux
 */

#ifndef TESTH_HPP_
#define TESTH_HPP_

#include <iostream>
#include <fstream>
#include <string>


class Test {
public:
	double x;
	friend std::ostream& operator << (std::ostream& os, const Test& p);
	inline Test();
	inline virtual ~Test();


};

inline std::ostream& operator << (std::ostream& os, const Test& p);
template <class V>
inline void save(const std::string& pname, const V& data);

#endif /* TESTH_HPP_ */
