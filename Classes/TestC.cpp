/*
 * TestC.cpp
 *
 *  Created on: 11 May 2015
 *      Author: zsoltlinux
 */

#ifndef TESTC_CPP_
#define TESTC_CPP_

#include "TestH.hpp"

Test::Test() {
	x=10;
}

Test::~Test() {}




std::ostream& operator << (std::ostream& os, const Test& p) {
 // Output to screen
	os << "Test:\t";
	os << p.x;
	return os;
}

template <class V>
void save(const std::string& pname, const V& data) {
	std::ofstream myfile;
	myfile.open(pname.c_str());
	myfile << data;
	myfile.close();
	std::cout << "====== File is saved! ======\nPathname: " << pname << std::endl;
}

#endif /* TESTC_CPP_ */


