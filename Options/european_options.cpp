#ifndef _EUROPEAN_OPTIONS_CPP
#define _EUROPEAN_OPTIONS_CPP
#include<iostream>
#include<cmath>

#include "C:\Zsolt\Uzlet\Tozsde\C++\Header\european_options.h"
#include "statistics.cpp"

// Constructors
	EuropeanOptions::EuropeanOptions() {init();}

// Copy operator
	EuropeanOptions::EuropeanOptions(const EuropeanOptions& opt) { copy(opt);}
	
	EuropeanOptions& EuropeanOptions::operator=(const EuropeanOptions& opt)
	{
		if (this == &opt) {return *this;}
		copy(opt);
		return *this;
	}
	
	
	// Methods
	void EuropeanOptions::init() {
		K = 100.0;
		r = 0.05;
		T = 1.0;
		S = 101.0;
		sigma = 0.2;
	}
	
	void EuropeanOptions::copy(const Options& ops) {
		K = ops.getK();
		r = ops.getr();
		T = ops.getT();
		S = ops.getS();
		sigma = ops.getsigma();
	}
	
	double EuropeanOptions::call_price() const {
		double sigmaT = sigma*sqrt(T);
		double d1 = (log(S/K) + sigma*sigma*0.5*T)/sigmaT;
		double d2 = d1 - sigmaT;
		StandardNormalDistribution N; 
		return (S*exp(-r*T)*N.cdf(d1)) - (K*exp(-r*T)*N.cdf(d2));
	}
	
	double EuropeanOptions::put_price() const {
		double sigmaT = sigma*sqrt(T);
		double d1 = (log(S/K) + sigma*sigma*0.5*T)/sigmaT;
		double d2 = d1 - sigmaT;
		StandardNormalDistribution N; 
		return (K*exp(-r*T)*N.cdf(-d2))-(S*exp(-r*T)*N.cdf(-d1)) ;
	}


#endif