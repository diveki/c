//#ifndef _NUMSTRINGCONVERTER_H
//#define _NUMSTRINGCONVERTER_H

#include<string>
#include<sstream>


template <typename T>
std::string NumberToString ( T Number )
{
	std::stringstream ss;
	ss << Number;
	return ss.str();
}

template <typename S>
S StringToNumber ( const std::string &Text )//Text not by const reference so that the function can be used with a 
{                               //character array as argument
	std::stringstream ss(Text);
	S result;
	return ss >> result ? result : 0;
}

//#endif