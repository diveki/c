#include<iostream>
#include<vector>
#include "european_options.cpp"

int main() {
	double K = 50.0;
	double r = 3.05;
	double T = 150;
	double S = 1021.0;
	double sigma = 0.32;
	
	EuropeanOptions d(K,r,S,sigma,T);
	EuropeanOptions c;
	
//	c=d;
	
	std::cout << c.put_price() << std::endl;
	return 0;
}