#ifndef _STANDARD_NORMAL_DISTRIBUTIONS_H
#define _STANDARD_NORMAL_DISTRIBUTIONS_H

#include<vector>
#include "distributions.h"

class StandardNormalDistribution: public Distributions {

public:
	// Constructors
	StandardNormalDistribution() {};
	virtual ~StandardNormalDistribution() {};

	// Methods
	double pdf(const double& _random);
	double cdf(const double& _random);
	double icdf(const double& _random);
	
	double mean() const {return 0.0;}
	double variance() const {return 1.0;} 
	double std() const {return 1.0;}
	
	// Obtain a sequence of random draws from this distribution
	void random_draws ( const std::vector<double>& uniform_draws,
		std::vector<double>& dist_draws ) ;

};
#endif