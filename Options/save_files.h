#ifndef _SAVE_FILES_H
#define _SAVE_FILES_H

#include<string>
#include<vector>

class SaveFiles {

private:
	std::string pathname;
	std::string filename;
	
public:
	SaveFiles() {pathname=""; filename="";};
	SaveFiles(const std::string& pn, const std::string& fn) {
		pathname = pn;
		filename = fn;
	};
	virtual ~SaveFiles() {};

	// Set Methods
	void set_pathname(const std::string& pn) {pathname = pn; }
	void set_filename(const std::string& fn) {filename = fn; }
	
	// Save Methods
	template <typename T>
		void save_file(const T& ots);
	template <typename S>
		void save_file(std::vector<S> & ots);
	template <typename T>
		void save_file(std::vector< std::vector<T> >& ots);
};

#endif