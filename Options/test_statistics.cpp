#include<iostream>
#include<vector>
#include "statistics.cpp"

int main() {
	double c = 0.975;
	
	StandardNormalDistribution a;
	
	std::cout << a.icdf(c) << std::endl;
	return 0;
}