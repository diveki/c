#ifndef _OPTIONS_H
#define _OPTIONS_H

class Options {
protected:
	double K;			// Strike price
	double r;			// free interest rate
	double sigma;		// volatility
	double d;			// dividend
	double T;			// maturity time
	double S;			// spot price
	
//	void init();		// initializes the options
	void copy(const Options& ops);

public:
	// Constructors
	Options() {};
	
	Options(const double& dd, const double& KK, const double& rr, const double& SS, const double& ssigma, const double& TT) {
			K = KK;
			r = rr;
			sigma = ssigma;
			d = dd;
			T = TT;
			S = SS;
			}; 
			
	Options(const double& KK, const double& rr, const double& SS, const double& ssigma, const double& TT) {
			K = KK;
			r = rr;
			sigma = ssigma;
			T = TT;
			S = SS;
			}; 
			
	Options(const Options& opt) ;		
	
	Options& operator=(const Options& opt) ;
	
	virtual ~Options() {};
	
	// Methods
	
	virtual double call_price() const = 0;
	virtual double put_price() const = 0;
	
	// Getter functions
	double getK() const {return K;}
	double getT() const {return T;}
	double getr() const {return r;}
	double getsigma() const {return sigma;}
	double getS() const {return S;}
	double getd() const {return d;}
	
	// Setter functions
	double setK(const double& KK) {K = KK;}
	double setT(const double& TT) {T = TT;}
	double setr(const double& rr) {r = rr;}
	double setsigma(const double& ssigma) {sigma = ssigma;}
	double setS(const double& SS) {S = SS;}
	double setd(const double& dd) {d = dd;}
	void init() {K=100;}		// initializes the options
	
};
#endif