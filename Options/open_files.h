#ifndef _OPEN_FILES_H
#define _OPEN_FILES_H

#include<string>
#include<vector>

class OpenFiles {

private:
	std::string pathname;
	std::string filename;
	
public:
	OpenFiles() {pathname=""; filename="";};
	OpenFiles(const std::string& pn, const std::string& fn) {
		pathname = pn;
		filename = fn;
	};
	virtual ~OpenFiles() {};

	// Set Methods
	void set_pathname(const std::string& pn) {pathname = pn; }
	void set_filename(const std::string& fn) {filename = fn; }
	
	// Open Methods
	template <typename T>
		std::vector<T> open_file();
/*	template <typename S>
		void open_file(std::vector<S> & ots);
	template <typename T>
		void open_file(std::vector< std::vector<T> >& ots);
*/
};

#endif