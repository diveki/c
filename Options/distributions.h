#ifndef _DISTRIBUTIONS_H
#define _DISTRIBUTIONS_H

#include<vector>

class Distributions {

public:
	// Constructors
	Distributions() {};
	virtual ~Distributions() {};

	// Methods
	virtual double pdf(const double& _random) = 0;
	virtual double cdf(const double& _random) = 0;
	virtual double icdf(const double& _random) = 0;
	
	virtual double mean() const = 0;
	virtual double variance() const = 0;
	virtual double std() const = 0;
	
	// Obtain a sequence of random draws from this distribution
	virtual void random_draws ( const std::vector<double>& uniform_draws,
		std::vector<double>& dist_draws ) = 0 ;

};
#endif