#ifndef _OPEN_FILES_CPP
#define _OPEN_FILES_CPP

#include<iostream>
#include<fstream>
#include<string>
#include<vector>
#include "C:\Zsolt\Uzlet\Tozsde\C++\Header\open_files.h"
#include "C:\Zsolt\Uzlet\Tozsde\C++\Header\numstringconverter.h"

std::string fullname, line;


template <typename T>
	// Save Methods
	std::vector<T> OpenFiles::open_file() {
		std::vector<T> vec;
		if (pathname.size()==0) {fullname = filename;}
		try {
			if (filename.size()==0) {throw "Define a filename!";}
			fullname = pathname+"\\"+filename;     // Function will be needed for this
			std::cout << "Full path of loading:" << std::endl;
			std::cout << fullname << std::endl;
			
			std::ifstream myfile (fullname.c_str());
			if (myfile.is_open())
			{
				int i=0;
				while ( getline (myfile,line) )
				{
					vec.push_back (StringToNumber<T>(line));

				}
				myfile.close();
			}
			else std::cout << "Unable to open file"; 
			
			std::cout << "Loading is done!" << std::endl;
		} 
		catch (const char* msg) {
			std::cout << "An error has occured:" << std::endl;
			std::cerr << msg << std::endl; 
		}
		return vec;
	};

/*
template <typename T>
	// Save Methods
	void SaveFiles::save_file(std::vector< std::vector<T> >& ots) {

		std::vector< std::vector<T> > objectToSave = ots;  
		if (pathname.size()==0) {fullname = filename;}
		try {
			if (filename.size()==0) {throw "Define a filename!";}
			std:: string test;
			fullname = pathname+"\\"+filename;     // Function will be needed for this
			std::cout << "Full path of saving:" << std::endl;
			std::cout << fullname << std::endl;
			
			std::ofstream ofs;
			ofs.open (fullname.c_str(), std::ofstream::out | std::ofstream::app);
			
			for ( std::vector<std::vector<int> >::size_type i = 0; i < objectToSave.size(); i++ )
			{
				for ( std::vector<int>::size_type j = 0; j < objectToSave[i].size(); j++ )
				{
					ofs << objectToSave[i][j] << ' ';
				}
				ofs << char(13) << std::endl;
			}
			
			ofs.close();

			std::cout << "Saving is done" << std::endl;
		} 
		catch (const char* msg) {
			std::cout << "An error has occured:" << std::endl;
			std::cerr << msg << std::endl; 
		}
	};

	
template <typename S>
	// Save Methods
void SaveFiles::save_file(std::vector<S> & ots) {
		std::vector<S> objectToSave = ots;  
		if (pathname.size()==0) {fullname = filename;}
		try {
			if (filename.size()==0) {throw "Define a filename!";}
			std:: string test;
			fullname = pathname+"\\"+filename;     // Function will be needed for this
			std::cout << "Full path of saving:" << std::endl;
			std::cout << fullname << std::endl;
			std::cout << "Vector content: " << objectToSave[0] << std::endl;

			std::ofstream ofs;
			ofs.open (fullname.c_str(), std::ofstream::out | std::ofstream::binary);

			for (int i = 0; i < objectToSave.size(); ++i)
			{	ofs << objectToSave[i] ;
				ofs << char(13) << std::endl;
			}

			ofs.close();

			std::cout << "Saving is done" << std::endl;
		} 
		catch (const char* msg) {
			std::cout << "An error has occured:" << std::endl;
			std::cerr << msg << std::endl; 
		}
	};
*/
#endif