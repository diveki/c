#ifndef _EUROPEAN_OPTIONS_H
#define _EUROPEAN_OPTIONS_H
#include<iostream>
#include "options.h"


class EuropeanOptions: public Options {
private:
	
	void init();		// initializes the options
	void copy(const Options& ops);

public:
	// Constructors
	EuropeanOptions();
	
	EuropeanOptions(const double& dd, const double& KK, const double& rr, const double& SS, const double& ssigma, const double& TT) {
			K = KK;
			r = rr;
			sigma = ssigma;
			d = dd;
			T = TT;
			S = SS;
			}; 
			
	EuropeanOptions(const double& KK, const double& rr, const double& SS, const double& ssigma, const double& TT) {
			K = KK;
			r = rr;
			sigma = ssigma;
			T = TT;
			S = SS;
			}; 
			
	virtual ~EuropeanOptions() {};
	
	// Copy operator
	EuropeanOptions(const EuropeanOptions& opt);
	
	EuropeanOptions& operator=(const EuropeanOptions& opt);
	
	// Methods
	
	double call_price() const ;
	double put_price() const ;
};
#endif