#include <iostream>
#include <string>
#include <vector>
//#include "C:\Zsolt\Uzlet\Tozsde\C++\Header\lin_con_gen.h"
#include "lin_con_gen.cpp"
#include "save_files.cpp"

int main(int argc, char **argv) {
	// Set the i n i t i a l seed and the d imens i ona l i t y of the RNG
//	unsigned long init_seed = 503200.0;
	unsigned long init_seed = 652635.0;
	unsigned long num_draws = 300;
	std::string path="C:\\Zsolt\\Uzlet\\Tozsde\\C++\\Code",filename="normaldistr.txt";
	std::string name;

	std::vector<double> random_draws(num_draws, 0.0);
	
	SaveFiles<std::vector<double> > e;
	// Create the LCG o b j e c t and c r e a t e the random uni form draws
	// on the open i n t e r v a l (0 ,1)

	LinearCongruentialGenerator lcg(num_draws, init_seed);
	lcg.get_uniform_draws(random_draws);
	// Output the random draws to the c ons o l e / s t d o u t
	for (unsigned long i = 0; i < num_draws; i++) {
		std::cout << i << " " << random_draws[i] << std::endl;
	}
	
// Saving
	e.set_pathname(path);
	e.set_filename(filename);
	e.set_object_to_save(random_draws);
	e.save_file();
	
//	std::getline(std::cin, name);
	return 0;
}