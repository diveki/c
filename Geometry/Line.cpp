/*
 * Line.cpp
 *
 *  Created on: 12 May 2015
 *      Author: zsoltlinux
 */

#ifndef LINE_CPP_
#define LINE_CPP

#include "Line.hpp"
#include <vector>
#include <iostream>
#include <cmath>

template <class V>
Line<V>::Line() {
	// TODO Auto-generated constructor stub
	std::vector<double> x1(2,0.0);
	std::vector<double> x2(2,1.1);
	Point<double> p1(x1), p2(x2);
	this->NumberOfPoints = 5;
	Line::init(p1, p2, this->NumberOfPoints);
}

template <class V>
Line<V>::Line(Point<V>& p1, Point<V>& p2, const int& nop) {
	this->NumberOfPoints = nop;
	Line::init(p1, p2, nop);
}

template <class V>
Line<V>::Line(Line<V>& source) {
	this->NumberOfPoints = source.NumberOfPoints;
	Line::init(source.start,source.end,source.NumberOfPoints);
}

template <class V>
Line<V>::~Line() {
	// TODO Auto-generated destructor stub
	delete [] Larray;
}

template <class V>
void Line<V>::init(Point<V>& p1, Point<V> p2, const int& nop) {
	Larray = new Point<V>[nop];
	double fact;

	start = Line<V>::StartPoint(p1,p2);
	end = Line<V>::EndPoint(p1,p2);
	inclination = end.subtract(start);
	Larray[0] = start;

	for (int i = 1; i<nop; ++i) {
		fact = i/double(nop-1);
		Larray[i] = start.add(inclination.scalar(fact));
	}
}

template <class V>
Point<V> Line<V>::StartPoint(Point<V>& p1, Point<V>& p2) {
	if (p1.RadialLenght() == p2.RadialLenght())
		return p1;
	else if (p1.RadialLenght() > p2.RadialLenght())
		return p2;
	else
		return p1;
}

template <class V>
Point<V> Line<V>::GetPoint(const int& it) {
	if ((it > this->NumberOfPoints) || (it <= 0)) {
		std::string msg ="The range of 'it' is [1-NumberOfPoints] in the Line class!";
		Point<V>::error_msg(typeid(Line).name(),__func__,"int& it",msg);
		return this->Larray[0];
	}
	else {
		return this->Larray[it-1];
	}

}

template <class V>
Point<V> Line<V>::EndPoint(Point<V>& p1, Point<V>& p2) {
	if (p1.RadialLenght() == p2.RadialLenght())
		return p2;
	else if (p1.RadialLenght() > p2.RadialLenght())
		return p1;
	else
		return p2;
}

template <class V>
double Line<V>::LineLenght() {
	return this->inclination.RadialLenght();
}

template <class V>
void Line<V>::print() {
	std::cout << "====== Linesegment details ======" << std::endl;
	std::cout << "The line has " << this->NumberOfPoints << " points:" << std::endl;
	for (int i=0; i<this->NumberOfPoints; ++i) {
		std::cout << i+1 << ". ";
		this->Larray[i].print();
	}
}

template <class V>
std::ostream& operator << (std::ostream& os, Line<V>& p)
{ // Output to screen
	Point<V> tmp;
	for (int i=0; i<p.NumberOfPoints; ++i) {
		tmp = p.GetPoint(i+1);
		os << tmp << "\n";
	}
	return os;
}

#endif
