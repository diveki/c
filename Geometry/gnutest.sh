#!/bin/bash

# gnutest: testing how to start gnuplot and plot with it from c++

set terminal wxt
plot sin(x)
set output 'test.png'
pause -1
