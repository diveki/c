/*
 * Point.cpp
 *
 *  Created on: 5 May 2015
 *      Author: zsoltlinux
 */

#ifndef POINT_CPP_
#define POINT_CPP_

#include <fstream>
#include "Point.hpp"
#include <iostream>
#include <cmath>

template <class V>
Point<V>::Point() {
	// TODO Auto-generated constructor stub
	std::vector<double> xs (1,0.0);
	Point::init(xs);
}

template <class V>
Point<V>::Point(const std::vector<V>& xs) {
	Point::init(xs);
}

template <class V>
Point<V>::Point(const Point<V>& src) {
	Point<V>::init(src.x);
}

template <class V>
Point<V>::~Point() {
	// TODO Auto-generated destructor stub
}

template <class V>
int Point<V>::Size() const {
	return this->size;
}

template <class V>
Point<V>& Point<V>::GetOrigin() {
	return Origin;
}

template <class V>
V Point<V>::get(const int& option) {
	if ((option>0) && (option <= this->Size()))
		return x[option-1];
	else {
		std::string msg = "'option' should be a positive integer and be smaller or equal than the dimension of the Point!\nThe returned value by this function is 0.";
		Point::error_msg(typeid(Point).name(),__func__,"option",msg);
		ermsg = "error";
		return 0.;
	}
}

template <class V>
void Point<V>::set(const int& option, const V& xs) {
	if ((option>0) && (option <= this->Size()))
			x[option-1] = xs;
		else {
			std::string msg ="'option' should be a positive integer and be smaller or equal than the dimension of 'xs'!";
			Point::error_msg(typeid(Point).name(),__func__,"option",msg);
		}
}

// Methods
template <class V>
int Point<V>::PointDim() {
	return this->Size();
}

template <class V>
Point<V> Point<V>::add(Point<V> p) {
	return PointFunctions(*this,p,addition);
}

template <class V>
Point<V> Point<V>::subtract(const Point<V>& p) {
	return PointFunctions(*this,p,subtraction);
}

template <class V>
Point<V> Point<V>::scale(const Point<V>& p) {
	return PointFunctions(*this,p,multiplication);
}
template <class V>
Point<V> Point<V>::midpoint(const Point<V>& p) {
	return PointFunctions(*this,p,midpoint_cal);
}
template <class V>
Point<V> Point<V>::scalar(const V& lambda) {
	std::vector<V> tmp (this->Size(),0.0);
	for (int i=1; i<=this->Size(); ++i) {
		tmp[i-1] = scalar_prod(lambda, this->get(i));
	}
	return Point<V>(tmp);
}

template <class V>
std::ostream& operator << (std::ostream& os, Point<V>& p)
{ // Output to screen
	for (int i=1; i<=p.PointDim(); ++i) {
		if (i == 1)
			os << p.get(i);
		else
			os << "\t" << p.get(i);
	}
	return os;
}

template <class V>
Point<V> PointFunctions(Point<V> p1, Point<V> p2, V (*f)(V x, V y)) {
	int n=p1.PointDim();
	std::vector<V> tmp (n,0);
	if (n != p2.PointDim()) {
		std::string msg ="'Point p' should the same dimension as this-> Point!";
		p1.error_msg(typeid(Point<V>).name(),__func__,"Point& p",msg);
		return Point<V>(tmp);
	}
	else {
		for (int i = 1; i <= n; ++i) {
			tmp[i-1] = (*f)(p1.get(i),p2.get(i));
		}
		return Point<V>(tmp);
	}
}

template <class V>
V addition(V x, V y) { return x + y; }

template <class V>
V subtraction(V x, V y) { return x - y; }
template <class V>
V multiplication(V x, V y) { return x * y; }
template <class V>
V midpoint_cal(V x, V y) { return (x + y)*0.5; }
template <class V>
V scalar_prod(const V& lambda, V x) { return lambda*x; }

template <class V>
void Point<V>::print() {
		std::cout << "Point: ( ";
		for (int j=0; j<this->Size(); ++j)
		{
			if (j == 0)
				std::cout << this->get(j+1);
			else
				std::cout << ", " << this->get(j+1);
		}
		std::cout << " )" << std::endl;
}

template <class V>
V Point<V>::RadialLenght() {
	V sum = 0.0;
	for (int i=1; i<=this->Size(); ++i)
		sum = sum + std::pow(this->get(i),2);
	return std::sqrt(sum);
}

#endif
