/*
 * TestPoint.cpp
 *
 *  Created on: 5 May 2015
 *      Author: zsoltlinux
 */

#include<iostream>
#include<typeinfo>
#include "Point.cpp"
#include "Line.cpp"


int main () {
	// Initiation of a 2d and a 3d coordinates
	std::vector<double> x (2, 10.2);
	std::vector<double> y (3, 3);
	std::string fn = "test.txt";	// creating file name for saving data

	x.push_back(13.4);				// extending the 2d to 3d coordinate
	// Instantiation of 3d points
	Point<double> p1(x);
	Point<double> p2(p1);
	Point<double> p4(y);

	//Changing the coordinates of point p1
	p1.set(1,3.2);
	p1.set(2,4.6);
	p1.set(3,5.5);

	// Creating a line from 2 endpoints: p1, p2 and with 8 points in between (in total 10 points)
	Line<double> l2(p1,p2,10);

	// instantiation of a plotting object for the Line
	Gnuplot gnu(l2);				// the instantiation saves the points of the line into data.txt
	gnu + "plot 'data.txt' using 1:2 with lines";			// adding gnuplot commands to plot the line
	gnu.ExportScript("test.gp");							// saving the gnuplot script for plot to test.gp
	gnu.Plot("test.gp");									// plotting the line
	mysave(fn,l2);											// saving the line into a file

	return 0;
}


