/*
 * Geometry.hpp
 *
 *  Created on: 5 May 2015
 *      Author: zsoltlinux
 */

#ifndef GEOMETRY_HPP_
#define GEOMETRY_HPP_

template <class V>
class Geometry {
public:
//	Geometry();
//	virtual ~Geometry();
	// Methods
	virtual void set(const int& option, const V& xs) = 0;
	virtual V get(const int& option) = 0;
	virtual void print() = 0;
};

#endif /* GEOMETRY_HPP_ */
