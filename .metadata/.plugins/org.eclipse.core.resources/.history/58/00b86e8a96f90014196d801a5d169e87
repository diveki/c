/*
 * Point.hpp
 *
 *  Created on: 5 May 2015
 *      Author: zsoltlinux
 */

#ifndef POINT_HPP_
#define POINT_HPP_

#include "Geometry.hpp"
#include "../Classes/ErrorClass.hpp"
#include "../Classes/SaveData.cpp"
#include <vector>
#include <typeinfo>

template <class V>
class Point: public Geometry<V>, public ErrorClass
{
//	friend std::ostream& operator << (std::ostream& os, Point& p);
private:
	// Parameters
	std::vector<V> x;
	int size;


	// Methods
	inline void init(const std::vector<V>& xs) { x = xs; ermsg = ""; size = xs.size(); }
	static Point<V> Origin;

public:
	std::string ermsg;
	inline Point();
	inline Point(const std::vector<V>& x);
	inline Point(const Point<V>& src);											// copy

	inline virtual ~Point();												// destructor

	// Setters

	inline int Size() const; // { return size; }
	inline void set(const int& option, const V& xs);
	inline V get(const int& option) ;
	inline static Point& GetOrigin();

	// Methods
	inline void print();
	inline int PointDim();
	inline Point<V> add(Point<V> p);
	inline Point<V> subtract(const Point<V>& p);
	inline Point<V> scale(const Point<V>& p);
	inline Point<V> midpoint(const Point<V>& p);
	inline Point<V> scalar(const V& lambda);

};

template <class V>
inline std::ostream& operator << (std::ostream& os, Point<V>& p);
template <class V>
inline Point<V>& operator = (const Point<V>& p);
template <class V>
inline Point<V> PointFunctions(Point<V> p1, Point<V> p2, V (*f)(V x, V y));
template <class V>
inline V addition(V x, V y);
template <class V>
inline V subtraction(V x, V y);
template <class V>
inline V multiplication(V x, V y);
template <class V>
inline V midpoint_cal(V x, V y);
template <class V>
inline V scalar_prod(const V& lambda, V x);


#endif /* POINT_HPP_ */
